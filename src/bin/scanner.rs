use lox_rs::scanner::{Scanner, TokenType};

pub fn main() {
    let path = std::env::args().nth(1).expect("path to source file");
    let source = std::fs::read(path).expect("lox source code file at path");

    let scanner = Scanner::new(source.into());

    let mut line = 0;
    for token in scanner.take_while(|t| t.r#type != TokenType::Eof) {
        if token.line == line {
            print!("   | ");
        } else {
            print!("{:4} ", token.line);
            line = token.line;
        }

        println!("{:?} {:?}", token.r#type, token.lexeme);
    }
}
