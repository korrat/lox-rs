use tracing_error::ErrorLayer;

use {
    color_eyre::eyre::{Result, WrapErr as _},
    lox_rs::vm::{InterpretError, VM},
    rustyline::{error::ReadlineError, Editor},
    std::{
        fs,
        path::{Path, PathBuf},
    },
    structopt::StructOpt,
    tracing::{error, Level},
};

fn run_file(path: &Path) -> Result<()> {
    let source = fs::read(path).context("reading source file")?;

    let mut vm = VM::new();
    match vm.interpret(source.into()) {
        Err(error) => {
            error!("{}", error);
            std::process::exit(match error {
                InterpretError::Compile { .. } => 65,
                InterpretError::Runtime { .. } => 70,
            });
        }

        Ok(()) => Ok(()),
    }
}

fn repl() -> Result<()> {
    let mut editor = Editor::<()>::new();
    let mut vm = VM::new();
    loop {
        let line = match editor.readline("> ") {
            Err(ReadlineError::Eof | ReadlineError::Interrupted) => {
                return Ok(());
            }
            Err(error) => {
                return Err(error).wrap_err("reading repl input");
            }

            Ok(line) => line,
        };

        if let Err(error) = vm.interpret(line.into()) {
            error!("{}", error);
        }
    }
}

fn run(matches: Options) -> Result<()> {
    matches.input.map_or_else(repl, |file| run_file(&file))
}

#[derive(Debug, StructOpt)]
#[structopt(name = "lox-rs")]
struct Options {
    #[structopt(short, long, about = "enable informational messages")]
    verbose: bool,

    #[structopt(
        short,
        long,
        about = "limit output to error messages",
        conflicts_with = "verbose"
    )]
    quiet: bool,

    input: Option<PathBuf>,
}

fn install_tracing(options: &Options) {
    use tracing_subscriber::{
        filter::EnvFilter, fmt, layer::SubscriberExt as _, util::SubscriberInitExt as _,
    };

    let fmt = fmt::layer()
        .without_time()
        .with_writer(std::io::stderr)
        .pretty();
    let env_filter = EnvFilter::try_from_default_env()
        .or_else(|_| {
            EnvFilter::try_new(
                (if options.verbose {
                    Level::INFO
                } else if options.quiet {
                    Level::ERROR
                } else {
                    Level::WARN
                })
                .as_str(),
            )
        })
        .unwrap();

    tracing_subscriber::registry()
        .with(env_filter)
        .with(fmt)
        .with(ErrorLayer::default())
        .init();
}

pub fn main() {
    let options = Options::from_args();
    install_tracing(&options);

    color_eyre::install().unwrap();

    if let Err(error) = run(options) {
        error!("{:?}", error);
        std::process::exit(64);
    }
}
