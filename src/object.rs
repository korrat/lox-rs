use {
    crate::{chunk::Chunk, value::Value},
    bytes::Bytes,
    std::{fmt::Display, time::UNIX_EPOCH},
    string_cache::DefaultAtom,
};

#[derive(Debug)]
pub struct Function {
    name: DefaultAtom,
    arity: usize,
    chunk: Chunk,
}

impl Function {
    pub fn new(name: DefaultAtom, arity: usize, chunk: Chunk) -> Self {
        Self { name, arity, chunk }
    }

    pub fn arity(&self) -> usize {
        self.arity
    }

    pub fn chunk(&self) -> &Chunk {
        &self.chunk
    }

    pub fn code(&self) -> Bytes {
        self.chunk.code()
    }

    pub fn get_constant(&self, index: usize) -> &Value {
        &self.chunk.constants()[index]
    }

    pub fn get_line(&self, offset: usize) -> usize {
        self.chunk.get_line(offset)
    }

    pub fn name(&self) -> &DefaultAtom {
        &self.name
    }
}

impl Display for Function {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if self.name.is_empty() {
            "<script>".fmt(f)
        } else {
            format_args!("<fn {}>", self.name).fmt(f)
        }
    }
}

#[derive(Debug)]
pub enum NativeFunction {
    Clock,
}

impl NativeFunction {
    pub fn call(&self, _arguments: &[Value]) -> Value {
        match self {
            Self::Clock => UNIX_EPOCH
                .elapsed()
                .map(|d| d.as_secs_f64())
                .unwrap_or(0.0)
                .into(),
        }
    }
}
