use {
    crate::{
        chunk::OpCode,
        compiler::{self, AggregateError},
        object::{Function, NativeFunction},
        value::Value,
    },
    bytes::{Buf as _, Bytes},
    if_chain::if_chain,
    indexmap::IndexMap,
    snafu::{ResultExt as _, Snafu},
    std::{
        convert::TryFrom as _,
        io::{Cursor, Seek as _, SeekFrom},
        rc::Rc,
    },
    string_cache::DefaultAtom,
    tap::Tap as _,
    tracing::warn,
};

// TODO consider implementing operations directly on `Value`
macro_rules! binary_op {
    ($self:ident, $op:tt) => {
        let stack = $self.stack.as_mut_slice();
        if let [.., Value::Number(left), Value::Number(right)] = stack {
            let new = (*left $op *right).into();
            $self.stack.pop();
            *$self.stack.last_mut().unwrap() = new;
        } else {
            return $self.runtime_error("operands must be numbers");
        }
    };
}

#[derive(Debug, Snafu)]
pub enum InterpretError {
    #[snafu(display("compiler error:\n\t{}", source))]
    Compile { source: AggregateError },
    #[snafu(display("runtime error: {}", message))]
    Runtime { message: String },
}

const FRAME_MAX: usize = 64;
const STACK_MAX: usize = FRAME_MAX * 256;

#[derive(Debug)]
pub struct CallFrame {
    function: Rc<Function>,
    ip: Cursor<Bytes>,

    base: usize,
}

impl CallFrame {
    pub fn new(function: Rc<Function>, base: usize) -> Self {
        let ip = Cursor::new(function.code());

        Self { function, ip, base }
    }

    pub fn base(&self) -> usize {
        self.base
    }

    pub fn current_location(&self) -> (DefaultAtom, usize) {
        let name = self.function.name();
        let name = if name.is_empty() {
            "script".into()
        } else {
            name.clone()
        };
        let offset = self.ip.position() as usize;
        (name, self.function.get_line(offset))
    }

    pub fn get_constant(&mut self, long: bool) -> &Value {
        let index = self.get_index(long);
        self.function.get_constant(index)
    }

    pub fn get_index(&mut self, long: bool) -> usize {
        if long {
            (self.ip.get_uint_le(3) + 256) as usize
        } else {
            self.ip.get_u8() as usize
        }
    }

    pub fn jump(&mut self, offset: u16) {
        let offset: i64 = offset.into();
        self.ip
            .seek(SeekFrom::Current(offset))
            .expect("invalid seek destination");
    }

    pub fn r#loop(&mut self, offset: u16) {
        let offset: i64 = offset.into();
        self.ip
            .seek(SeekFrom::Current(-offset))
            .expect("invalid seek destination");
    }
}

#[derive(Debug)]
pub struct VM {
    call_frames: Vec<CallFrame>,

    // TODO grow this at some point
    // TODO implement stack sharing with CallFrames
    stack: Vec<Value>,

    globals: IndexMap<DefaultAtom, Option<Value>>,
}

impl VM {
    pub fn new() -> Self {
        Self::default().tap_mut(|vm| vm.define_native(NativeFunction::Clock))
    }

    pub fn insert_global(&mut self, name: DefaultAtom) -> usize {
        let entry = self.globals.entry(name);
        let index = entry.index();
        entry.or_default();

        index
    }

    pub fn interpret(&mut self, source: Bytes) -> Result<(), InterpretError> {
        let function = compiler::compile(self, source).context(Compile)?;

        self.stack.push(function.clone().into());
        self.call(function, 0, 0)?;

        self.run()
    }

    fn call(
        &mut self,
        function: Rc<Function>,
        arg_count: usize,
        base: usize,
    ) -> Result<(), InterpretError> {
        if function.arity() != arg_count {
            return self.runtime_error(&format_args!(
                "expected {} arguments but got {}",
                function.arity(),
                arg_count
            ));
        }

        if self.call_frames.len() == FRAME_MAX {
            return self.runtime_error("stack overflow");
        }

        self.call_frames.push(CallFrame::new(function, base));
        Ok(())
    }

    fn call_value(&mut self, arg_count: usize) -> Result<(), InterpretError> {
        let base = (self.stack.len() - arg_count) - 1;
        match &self.stack[base] {
            Value::Function(callee) => {
                let callee = callee.clone();
                self.call(callee, arg_count, base)
            }

            Value::NativeFunction(callee) => {
                let arguments = &self.stack[base + 1..];
                let result = callee.call(arguments);
                self.stack.truncate(base + 1);
                self.stack[base] = result;
                Ok(())
            }

            _ => self.runtime_error("can only call functions and classes"),
        }
    }

    fn define_native(&mut self, function: NativeFunction) {
        let index = self.insert_global("clock".into());
        self.globals[index].replace(function.into());
    }

    fn frame(&mut self) -> &mut CallFrame {
        self.call_frames.last_mut().unwrap()
    }

    fn get_global(&mut self, long: bool) -> &mut Option<Value> {
        let index = self.frame().get_index(long);
        &mut self.globals[index]
    }

    fn get_local(&mut self, long: bool) -> &mut Value {
        let frame = self.frame();
        let offset = frame.base();
        let index = frame.get_index(long);

        &mut self.stack[offset + index]
    }

    fn run(&mut self) -> Result<(), InterpretError> {
        loop {
            #[cfg(feature = "debug-trace-execution")]
            {
                eprint!("          ");
                for value in &self.stack {
                    eprint!("[ {} ]", value);
                }
                eprintln!();

                let frame = self.frame();
                crate::debug::disassemble_instruction(
                    &mut std::io::stderr(),
                    frame.function.chunk(),
                    frame.ip.position() as usize,
                );
            }

            let instruction = self.frame().ip.get_u8();
            match OpCode::try_from(instruction) {
                Ok(code) => match code {
                    OpCode::Constant | OpCode::ConstantLong => {
                        let value = self.frame().get_constant(code.is_long()).clone();
                        self.stack.push(value);
                    }

                    OpCode::Nil => {
                        self.stack.push(Value::Nil);
                    }
                    OpCode::True => {
                        self.stack.push(true.into());
                    }
                    OpCode::False => {
                        self.stack.push(false.into());
                    }

                    OpCode::Pop => {
                        self.stack.pop();
                    }

                    OpCode::GetLocal | OpCode::GetLocalLong => {
                        let local = self.get_local(code.is_long()).clone();
                        self.stack.push(local);
                    }

                    OpCode::SetLocal | OpCode::SetLocalLong => {
                        let value = self.stack.last().unwrap().clone();
                        let local = self.get_local(code.is_long());
                        *local = value;
                    }

                    OpCode::GetGlobal | OpCode::GetGlobalLong => {
                        let value = self.get_global(code.is_long());
                        if let Some(value) = value {
                            let value = value.clone();
                            self.stack.push(value);
                        } else {
                            return self.runtime_error("undefined variable");
                        }
                    }

                    OpCode::DefineGlobal | OpCode::DefineGlobalLong => {
                        let inner = self.stack.pop().unwrap();
                        let value = self.get_global(code.is_long());
                        value.replace(inner);
                    }

                    OpCode::SetGlobal | OpCode::SetGlobalLong => {
                        let inner = self.stack.last().unwrap().clone();
                        let value = self.get_global(code.is_long());
                        if let Some(value) = value {
                            *value = inner;
                        } else {
                            return self.runtime_error("undefined variable");
                        }
                    }

                    OpCode::Equal => {
                        let right = self.stack.pop().unwrap();
                        let left = self.stack.last_mut().unwrap();

                        *left = (*left == right).into();
                    }
                    OpCode::Greater => {
                        binary_op!(self, >);
                    }
                    OpCode::Less => {
                        binary_op!(self, <);
                    }

                    OpCode::Add => {
                        let stack = self.stack.as_mut_slice();
                        if_chain! {
                            if let [.., Value::String(left), Value::String(right)] = stack;
                            then {
                                *left = [left.as_ref(), right.as_ref()].concat().into();
                                self.stack.pop();
                                continue;
                            }
                        }

                        if let [.., Value::Number(left), Value::Number(right)] = stack {
                            *left += *right;
                            self.stack.pop();
                        } else {
                            return self.runtime_error("operands must be numbers or strings");
                        }
                    }
                    OpCode::Subtract => {
                        binary_op!(self, -);
                    }
                    OpCode::Multiply => {
                        binary_op!(self, *);
                    }
                    OpCode::Divide => {
                        binary_op!(self, /);
                    }

                    OpCode::Not => {
                        let left = self.stack.last_mut().unwrap();
                        *left = left.is_falsey().into();
                    }

                    OpCode::Negate => {
                        if let Value::Number(value) = self.stack.last_mut().unwrap() {
                            *value = -*value;
                        } else {
                            return self.runtime_error("operand must be a number");
                        }
                    }

                    OpCode::Print => {
                        println!("{}", self.stack.pop().unwrap());
                    }

                    OpCode::Jump => {
                        let offset = self.frame().ip.get_u16();
                        self.frame().jump(offset);
                    }

                    OpCode::JumpIfFalse => {
                        let offset = self.frame().ip.get_u16();
                        if self.stack.last().unwrap().is_falsey() {
                            self.frame().jump(offset);
                        }
                    }

                    OpCode::Loop => {
                        let offset = self.frame().ip.get_u16();
                        self.frame().r#loop(offset);
                    }

                    OpCode::Call => {
                        let arg_count = self.frame().get_index(code.is_long());
                        self.call_value(arg_count)?;
                    }

                    OpCode::Return => {
                        let result = self.stack.pop().unwrap();
                        let frame = self.call_frames.pop().unwrap();
                        if self.call_frames.is_empty() {
                            self.stack.pop();
                            return Ok(());
                        }

                        let base = frame.base();
                        self.stack.truncate(base + 1);
                        self.stack[base] = result;
                    }
                },
                Err(e) => warn!(?e, "unsupported op code"),
            }
        }
    }

    fn runtime_error<S>(&mut self, message: &S) -> Result<(), InterpretError>
    where
        S: ToString + ?Sized,
    {
        let message = message.to_string();

        let message = std::iter::once(message)
            .chain(self.call_frames.iter().rev().map(|frame| {
                let (name, line) = frame.current_location();
                format!("[line {}] in {}", line, name)
            }))
            .collect::<Vec<_>>()
            .join("\n\t");

        Runtime { message }.fail()
    }
}

impl Default for VM {
    fn default() -> Self {
        let call_frames = Vec::with_capacity(FRAME_MAX);
        let stack = Vec::with_capacity(STACK_MAX);
        let globals = Default::default();

        Self {
            call_frames,
            stack,
            globals,
        }
    }
}
