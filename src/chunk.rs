use {
    crate::value::Value,
    bytes::{BufMut as _, Bytes, BytesMut},
    if_chain::if_chain,
    num_enum::{IntoPrimitive, TryFromPrimitive},
    std::convert::TryInto,
};

#[derive(Debug, Default)]
pub struct Chunk {
    code: Bytes,
    constants: Box<[Value]>,
    lines: Box<[(usize, usize)]>,
}

impl Chunk {
    pub fn builder() -> ChunkBuilder {
        ChunkBuilder::default()
    }

    pub fn code(&self) -> Bytes {
        self.code.clone()
    }

    pub fn constants(&self) -> &[Value] {
        &self.constants
    }

    pub fn get_line(&self, offset: usize) -> usize {
        let index = self.lines.partition_point(|(o, _)| o <= &offset);
        self.lines[index - 1].1
    }
}

#[derive(Debug, Default)]
pub struct ChunkBuilder {
    code: BytesMut,
    constants: Vec<Value>,
    lines: Vec<(usize, usize)>,
}

impl ChunkBuilder {
    pub fn add_constant(&mut self, value: Value) -> usize {
        // ! this is O(n^2) in the number of constants added
        // TODO add a caching layer value -> index
        self.constants
            .iter()
            .position(|c| c == &value)
            .unwrap_or_else(|| {
                self.constants.push(value);
                self.constants.len() - 1
            })
    }

    pub fn current_offset(&self) -> usize {
        self.code.len()
    }

    pub fn finish(&mut self) -> Chunk {
        let code = self.code.split().freeze();
        let constants = std::mem::take(&mut self.constants).into_boxed_slice();
        let lines = std::mem::take(&mut self.lines).into_boxed_slice();

        Chunk {
            code,
            constants,
            lines,
        }
    }

    pub fn patch_jump(&mut self, offset: usize) -> Result<&mut Self, &'static str> {
        let jump = ((self.code.len() - offset) - 2)
            .try_into()
            .map_err(|_| "too much code to jump over")?;

        (&mut self.code[offset..]).put_u16(jump);
        Ok(self)
    }

    pub fn write_call(&mut self, argument_count: usize, line: usize) -> &mut Self {
        self.write_indexed(OpCode::Call, argument_count, line)
    }

    pub fn write_constant<V: Into<Value>>(&mut self, value: V, line: usize) -> &mut Self {
        let index = self.add_constant(value.into());

        self.write_indexed(OpCode::Constant, index, line)
    }

    pub fn write_indexed(&mut self, code: OpCode, index: usize, line: usize) -> &mut Self {
        let long = code.long().expect("invalid opcode");

        self.write_indexed_raw(code, long, index, line)
    }

    pub fn write_jump(&mut self, code: OpCode, line: usize) -> usize {
        self.write_operation(code, line);

        let current_offset = self.code.len();
        self.code.put_u16(u16::MAX);
        current_offset
    }

    pub fn write_loop(
        &mut self,
        code: OpCode,
        start: usize,
        line: usize,
    ) -> Result<(), &'static str> {
        self.write_operation(code, line);

        let offset = ((self.current_offset() - start) + 2)
            .try_into()
            .map_err(|_| "loop body too large")?;
        self.code.put_u16(offset);
        Ok(())
    }

    pub fn write_operation(&mut self, code: OpCode, line: usize) -> &mut Self {
        if_chain! {
            if let Some(&(_, last_line)) = self.lines.last();
            if last_line == line;
            then {
                // nothing to do
            }
            else {
                self.lines.push((self.code.len(), line))
            }
        };

        self.code.put_u8(code.into());
        self
    }

    fn write_indexed_raw(
        &mut self,
        short: OpCode,
        long: OpCode,
        index: usize,
        line: usize,
    ) -> &mut Self {
        const MAX_ITEMS: usize = (1 << 24) + 255;

        match index {
            0..=255 => {
                self.write_operation(short, line);

                self.code.put_u8(index as u8);
            }
            256..=MAX_ITEMS => {
                self.write_operation(long, line);
                self.code.put_uint_le((index - 256) as u64, 3);
            }
            _ => unreachable!(),
        }

        self
    }
}

#[derive(Copy, Clone, Debug, IntoPrimitive, TryFromPrimitive)]
#[repr(u8)]
pub enum OpCode {
    Constant,
    ConstantLong,
    Nil,
    True,
    False,
    Pop,
    GetLocal,
    GetLocalLong,
    SetLocal,
    SetLocalLong,
    GetGlobal,
    GetGlobalLong,
    DefineGlobal,
    DefineGlobalLong,
    SetGlobal,
    SetGlobalLong,
    Equal,
    Greater,
    Less,
    Add,
    Subtract,
    Multiply,
    Divide,
    Not,
    Negate,
    Print,
    Jump,
    JumpIfFalse,
    Loop,
    Call,
    Return,
}

impl OpCode {
    pub fn is_long(self) -> bool {
        matches!(
            self,
            OpCode::ConstantLong
                | OpCode::GetLocalLong
                | OpCode::SetLocalLong
                | OpCode::GetGlobalLong
                | OpCode::DefineGlobalLong
                | OpCode::SetGlobalLong
        )
    }

    pub fn long(self) -> Option<Self> {
        match self {
            OpCode::Constant => Some(OpCode::ConstantLong),
            OpCode::GetLocal => Some(OpCode::GetLocalLong),
            OpCode::SetLocal => Some(OpCode::SetLocalLong),
            OpCode::GetGlobal => Some(OpCode::GetGlobalLong),
            OpCode::DefineGlobal => Some(OpCode::DefineGlobalLong),
            OpCode::SetGlobal => Some(OpCode::SetGlobalLong),

            // Calls reuse the index to specify the argument count,
            // which is guaranteed to be less than 256 implement.
            // Therefore, this is fine
            OpCode::Call => Some(OpCode::Call),

            _ => None,
        }
    }
}

#[cfg(test)]
mod that {
    use super::*;

    #[test]
    #[should_panic]
    fn empty_chunks_are_fine() {
        let chunk = Chunk::default();

        chunk.get_line(0);
    }

    #[test]
    fn extreme_offsets_are_fine() {
        let chunk = Chunk::builder()
            .write_constant(0.4, 3)
            .write_constant(0.6, 3)
            .write_operation(OpCode::Add, 3)
            .write_operation(OpCode::Return, 1)
            .finish();

        assert_eq!(chunk.get_line(0), 3);
        assert_eq!(chunk.get_line(5), 1);
    }
}
