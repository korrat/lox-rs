use {
    crate::object::{Function, NativeFunction},
    std::{fmt::Display, rc::Rc},
    string_cache::DefaultAtom,
};

#[derive(Clone, Debug)]
pub enum Value {
    Boolean(bool),
    Function(Rc<Function>),
    NativeFunction(Rc<NativeFunction>),
    Nil,
    Number(f64),
    String(DefaultAtom),
}

impl Value {
    pub fn is_falsey(&self) -> bool {
        matches!(self, Self::Nil | Self::Boolean(false))
    }
}

impl Display for Value {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        match self {
            Self::Boolean(inner) => inner.fmt(f),
            Self::Function(inner) => inner.fmt(f),
            Self::NativeFunction(_) => "<native fn>".fmt(f),
            Self::Nil => "nil".fmt(f),
            Self::Number(inner) => inner.fmt(f),
            Self::String(inner) => inner.fmt(f),
        }
    }
}

impl From<bool> for Value {
    fn from(other: bool) -> Self {
        Self::Boolean(other)
    }
}

impl From<DefaultAtom> for Value {
    fn from(other: DefaultAtom) -> Self {
        Self::String(other)
    }
}

impl From<f64> for Value {
    fn from(other: f64) -> Self {
        Self::Number(other)
    }
}

impl From<Rc<Function>> for Value {
    fn from(other: Rc<Function>) -> Self {
        Self::Function(other)
    }
}

impl From<NativeFunction> for Value {
    fn from(other: NativeFunction) -> Self {
        Self::NativeFunction(Rc::new(other))
    }
}

impl From<&str> for Value {
    fn from(other: &str) -> Self {
        Self::String(other.into())
    }
}

impl PartialEq for Value {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Self::Boolean(left), Self::Boolean(right)) => left == right,
            (Self::Function(left), Self::Function(right)) => Rc::ptr_eq(left, right),
            (Self::NativeFunction(left), Self::NativeFunction(right)) => Rc::ptr_eq(left, right),
            (Self::Nil, Self::Nil) => true,
            (Self::Number(left), Self::Number(right)) => left == right,
            (Self::String(left), Self::String(right)) => left == right,

            _ => false,
        }
    }
}

static_assertions::const_assert_eq!(std::mem::size_of::<Value>(), 16);
