use {
    crate::chunk::{Chunk, OpCode},
    bytes::Buf as _,
    std::{convert::TryFrom as _, io::Write},
    tracing::warn,
};

pub fn disassemble_chunk<W: Write>(out: &mut W, chunk: &Chunk, name: &str) {
    writeln!(out, "== {} ==", name).unwrap();

    let code = chunk.code();
    let mut offset = 0;
    while offset < code.len() {
        offset = disassemble_instruction(out, chunk, offset);
    }
}

pub fn disassemble_instruction<W: Write>(out: &mut W, chunk: &Chunk, offset: usize) -> usize {
    write!(out, "{:04} ", offset,).unwrap();

    let line = chunk.get_line(offset);
    if 0 < offset && chunk.get_line(offset - 1) == line {
        write!(out, "   | ")
    } else {
        write!(out, "{:4} ", line)
    }
    .unwrap();

    let code = chunk.code()[offset];
    match OpCode::try_from(code) {
        Ok(code) => match code {
            OpCode::Constant
            | OpCode::ConstantLong
            | OpCode::GetLocal
            | OpCode::GetLocalLong
            | OpCode::SetLocal
            | OpCode::SetLocalLong
            | OpCode::GetGlobal
            | OpCode::GetGlobalLong
            | OpCode::DefineGlobal
            | OpCode::DefineGlobalLong
            | OpCode::SetGlobal
            | OpCode::SetGlobalLong
            | OpCode::Call => indexed_instruction(out, code, chunk, offset),

            OpCode::Jump | OpCode::JumpIfFalse => jump_instruction(out, code, true, chunk, offset),
            OpCode::Loop => jump_instruction(out, code, false, chunk, offset),

            OpCode::Nil
            | OpCode::True
            | OpCode::False
            | OpCode::Pop
            | OpCode::Equal
            | OpCode::Greater
            | OpCode::Less
            | OpCode::Add
            | OpCode::Subtract
            | OpCode::Multiply
            | OpCode::Divide
            | OpCode::Not
            | OpCode::Negate
            | OpCode::Print
            | OpCode::Return => simple_instruction(out, code, offset),
        },
        Err(code) => {
            warn!("unsupported op code: '{}'", code);
            offset + 1
        }
    }
}

fn indexed_instruction<W: Write>(out: &mut W, name: OpCode, chunk: &Chunk, offset: usize) -> usize {
    let instruction = format!("{:?}", name);
    let mut code = &chunk.code()[offset + 1..];
    let index = if name.is_long() {
        (code.get_uint_le(3) + 256) as usize
    } else {
        code.get_u8().into()
    };

    if matches!(name, OpCode::Constant | OpCode::ConstantLong) {
        // for constant instructions, look up the value in the constant table
        let value = &chunk.constants()[index];
        writeln!(out, "{:16} {:4} '{}'", instruction, index, value)
    } else {
        writeln!(out, "{:16} {:4}", instruction, index)
    }
    .unwrap();

    offset + if name.is_long() { 4 } else { 2 }
}

fn jump_instruction<W: Write>(
    out: &mut W,
    name: OpCode,
    forward: bool,
    chunk: &Chunk,
    offset: usize,
) -> usize {
    let jump = (&chunk.code()[offset + 1..]).get_u16() as usize;
    let target = if forward {
        offset + 3 + jump
    } else {
        offset + 3 - jump
    };

    writeln!(
        out,
        "{:16} {:4} -> {}",
        format!("{:?}", name),
        offset,
        target
    )
    .unwrap();

    offset + 3
}

fn simple_instruction<W: Write>(out: &mut W, name: OpCode, offset: usize) -> usize {
    writeln!(out, "{:?}", name).unwrap();
    offset + 1
}
