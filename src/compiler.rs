use {
    crate::{
        chunk::{Chunk, ChunkBuilder, OpCode},
        debug,
        object::Function,
        scanner::{Scanner, Token, TokenType},
        value::Value,
        vm::VM,
    },
    bytes::Bytes,
    if_chain::if_chain,
    snafu::Snafu,
    std::{io, mem, rc::Rc},
    string_cache::DefaultAtom,
    tracing::trace,
};

#[derive(Debug, Snafu)]
#[snafu(display(
    "[line {}]{} {}",
    token.line,
    if TokenType::Eof == token.r#type {
        " at end:".to_string()
    } else if TokenType::Error == token.r#type {
        "".to_string()
    } else {
        let s = std::str::from_utf8(&token.lexeme).unwrap();
        format!(" at '{}':", s)
    },
    message
))]
pub struct ParserError {
    token: Token,
    message: String,
}

#[derive(Debug, Snafu)]
#[snafu(display("{}", inner.iter().map(ToString::to_string).collect::<Vec<_>>().join("\n\t")))]
pub struct AggregateError {
    inner: Box<[ParserError]>,
}

pub fn compile(vm: &mut VM, source: Bytes) -> Result<Rc<Function>, AggregateError> {
    let mut parser = Parser::new(vm, source);

    parser.advance();
    while !parser.r#match(TokenType::Eof) {
        parser.declaration();
    }

    parser.finish()
}

#[derive(Debug)]
struct FunctionBuilder {
    name: DefaultAtom,
    arity: usize,

    body: ChunkBuilder,
}

impl FunctionBuilder {
    pub fn new(name: DefaultAtom) -> Self {
        Self {
            name,
            arity: 0,

            body: Chunk::builder(),
        }
    }

    pub fn finish(mut self) -> Rc<Function> {
        let name = self.name;
        let chunk = self.body.finish();

        if cfg!(feature = "debug-print-code") {
            debug::disassemble_chunk(
                &mut io::stderr(),
                &chunk,
                if name.is_empty() { "code" } else { &name },
            );
        }

        Rc::new(Function::new(name, self.arity, chunk))
    }
}

#[derive(Debug)]
enum FunctionKind {
    Function,
    Script,
}

impl Default for FunctionKind {
    fn default() -> Self {
        Self::Script
    }
}

#[derive(Debug)]
struct Compiler {
    function: FunctionBuilder,
    kind: FunctionKind,

    locals: Vec<Local>,
    scope_depth: usize,
}

impl Compiler {
    pub fn new(kind: FunctionKind, name: DefaultAtom) -> Self {
        let mut local = Local::new(Token::new(
            TokenType::Identifier,
            Bytes::from_static(b""),
            0,
        ));
        local.depth.replace(0);
        let locals = vec![local];

        Self {
            function: FunctionBuilder::new(name),
            kind,

            locals,
            scope_depth: 0,
        }
    }

    pub fn add_local(&mut self, name: Token) {
        let local = Local::new(name);
        self.locals.push(local);
    }

    pub fn begin_scope(&mut self) {
        self.scope_depth += 1;
    }

    pub fn end_scope(&mut self) -> usize {
        self.scope_depth -= 1;

        let removed = self
            .locals
            .iter()
            .rev()
            .take_while(|local| {
                self.scope_depth
                    < local
                        .depth
                        .expect("should not be called in variable declaration")
            })
            .count();
        self.locals.truncate(self.locals.len() - removed);
        removed
    }

    pub fn find_local_in_scope(&self, name: &Token) -> bool {
        for local in self.locals.iter().rev() {
            if_chain! {
                if let Some(depth) = local.depth;
                if depth < self.scope_depth;
                then {
                    return false;
                }
            }

            if local.name.lexeme == name.lexeme {
                return true;
            }
        }

        false
    }

    pub fn finish(self) -> Rc<Function> {
        self.function.finish()
    }

    pub fn mark_initialized(&mut self) {
        if 0 == self.scope_depth {
            return;
        }

        self.locals
            .last_mut()
            .unwrap()
            .mark_initialized(self.scope_depth);
    }

    pub fn resolve_local(&self, name: &Bytes) -> Result<Option<usize>, &'static str> {
        for (index, local) in self.locals.iter().enumerate().rev() {
            if local.name.lexeme == name {
                if local.depth.is_none() {
                    return Err("can't read local variable in its own initializer");
                }
                return Ok(Some(index));
            }
        }

        Ok(None)
    }
}

#[derive(Clone, Debug)]
struct Local {
    name: Token,
    depth: Option<usize>,
}

impl Local {
    pub fn new(name: Token) -> Self {
        Self { name, depth: None }
    }

    pub fn mark_initialized(&mut self, depth: usize) {
        self.depth.replace(depth);
    }
}

#[derive(Debug)]
struct Parser<'vm> {
    vm: &'vm mut VM,

    scanner: Scanner,

    previous: Option<Token>,
    current: Option<Token>,

    errors: Vec<ParserError>,
    panic_mode: bool,

    compiler: Compiler,
}

impl<'vm> Parser<'vm> {
    fn new(vm: &'vm mut VM, source: Bytes) -> Self {
        Self {
            vm,

            scanner: Scanner::new(source),

            previous: None,
            current: None,

            errors: Vec::new(),
            panic_mode: false,

            compiler: Compiler::new(FunctionKind::Script, "".into()),
        }
    }

    fn advance(&mut self) {
        self.previous = self.current.take();

        loop {
            self.current = self.scanner.next();

            if_chain! {
                if let Some(token) = &self.current;
                if token.r#type != TokenType::Error;
                then {
                    return;
                }
            }

            let message = self.current().lexeme.clone();
            let message = std::str::from_utf8(&message).unwrap();

            self.error_at_current(message);
        }
    }

    #[tracing::instrument(level = "trace")]
    fn and(&mut self) {
        let end_jump = self.emit_jump(OpCode::JumpIfFalse);

        self.emit_operation(OpCode::Pop);
        self.parse_precedence(Precedence::And);

        self.patch_jump(end_jump);
    }

    #[tracing::instrument(level = "trace")]
    fn argument_list(&mut self) -> usize {
        let mut arg_count = 0;
        if !self.check(TokenType::RightParen) {
            loop {
                self.expression();
                if arg_count == 255 {
                    self.error("can't have more than 255 arguments");
                }
                arg_count += 1;
                if !self.r#match(TokenType::Comma) {
                    break;
                }
            }
        }

        self.consume(TokenType::RightParen, "expected ')' after arguments");
        arg_count
    }

    #[tracing::instrument(level = "trace")]
    fn binary(&mut self) {
        let operator = self.previous().r#type;
        let rule = Self::get_rule(operator);

        self.parse_precedence(rule.precedence.next());

        match operator {
            TokenType::BangEqual => self.emit_operations(&[OpCode::Equal, OpCode::Not]),
            TokenType::EqualEqual => self.emit_operation(OpCode::Equal),
            TokenType::Greater => self.emit_operation(OpCode::Greater),
            TokenType::GreaterEqual => self.emit_operations(&[OpCode::Less, OpCode::Not]),
            TokenType::Less => self.emit_operation(OpCode::Less),
            TokenType::LessEqual => self.emit_operations(&[OpCode::Greater, OpCode::Not]),
            TokenType::Plus => self.emit_operation(OpCode::Add),
            TokenType::Minus => self.emit_operation(OpCode::Subtract),
            TokenType::Star => self.emit_operation(OpCode::Multiply),
            TokenType::Slash => self.emit_operation(OpCode::Divide),

            _ => unreachable!(),
        }
    }

    #[tracing::instrument(level = "trace")]
    fn block(&mut self) {
        while !self.check(TokenType::RightBrace) && !self.check(TokenType::Eof) {
            self.declaration();
        }

        self.consume(TokenType::RightBrace, "expected '}' after block");
    }

    #[tracing::instrument(level = "trace")]
    fn call(&mut self) {
        let arg_count = self.argument_list();
        self.emit_call(arg_count);
    }

    fn check(&mut self, r#type: TokenType) -> bool {
        self.current().r#type == r#type
    }

    fn consume<S>(&mut self, r#type: TokenType, message: &S)
    where
        S: ToString + ?Sized,
    {
        if self.current().r#type == r#type {
            self.advance();
        } else {
            self.error_at_current(message);
        }
    }

    fn current(&self) -> &Token {
        self.current.as_ref().unwrap()
    }

    fn current_chunk(&mut self) -> &mut ChunkBuilder {
        &mut self.compiler.function.body
    }

    fn current_rule(&self) -> Rule<'vm> {
        Self::get_rule(self.current().r#type)
    }

    #[tracing::instrument(level = "trace")]
    fn declaration(&mut self) {
        if self.r#match(TokenType::Fun) {
            self.fun_declaration();
        } else if self.r#match(TokenType::Var) {
            self.var_declaration();
        } else {
            self.statement();
        }

        if self.panic_mode {
            self.synchronize();
        }
    }

    fn declare_variable(&mut self) {
        if self.compiler.scope_depth == 0 {
            return;
        }

        let name = self.previous().clone();

        if self.compiler.find_local_in_scope(&name) {
            self.error("a local variable with this name was already defined in this scope");
        }

        self.compiler.add_local(name);
    }

    fn define_variable(&mut self, index: usize) {
        if 0 < self.compiler.scope_depth {
            self.compiler.mark_initialized();
            return;
        }

        self.emit_indexed(OpCode::DefineGlobal, index);
    }

    fn emit_call(&mut self, arg_count: usize) {
        self.emit_indexed(OpCode::Call, arg_count);
    }

    fn emit_constant<V: Into<Value>>(&mut self, value: V) {
        let value = value.into();
        trace!(?value, "emitting constant");

        let line = self.previous().line;
        self.current_chunk().write_constant(value, line);
    }

    fn emit_indexed(&mut self, operation: OpCode, index: usize) {
        let line = self.previous().line;
        self.current_chunk().write_indexed(operation, index, line);
    }

    fn emit_jump(&mut self, code: OpCode) -> usize {
        let line = self.previous().line;
        self.current_chunk().write_jump(code, line)
    }

    fn emit_loop(&mut self, start: usize) {
        let line = self.previous().line;
        if let Err(error) = self.current_chunk().write_loop(OpCode::Loop, start, line) {
            self.error(error);
        }
    }

    fn emit_operation(&mut self, operation: OpCode) {
        trace!(?operation, "emitting operator");

        let line = self.previous().line;
        self.current_chunk().write_operation(operation, line);
    }

    fn emit_operations(&mut self, operation: &[OpCode]) {
        for operation in operation {
            self.emit_operation(*operation);
        }
    }

    fn emit_return(&mut self) {
        self.emit_operations(&[OpCode::Nil, OpCode::Return]);
    }

    fn error<S>(&mut self, message: &S)
    where
        S: ToString + ?Sized,
    {
        if let Some(token) = self.previous.clone() {
            self.error_at(token, message);
        }
    }

    fn error_at<S>(&mut self, token: Token, message: &S)
    where
        S: ToString + ?Sized,
    {
        if !self.panic_mode {
            self.panic_mode = true;
            let message = message.to_string();
            let error = ParserContext { token, message }.build();
            trace!(?self, ?error, "producing error");

            self.errors.push(error);
        }
    }

    fn error_at_current<S>(&mut self, message: &S)
    where
        S: ToString + ?Sized,
    {
        if let Some(token) = self.current.clone() {
            self.error_at(token, message);
        }
    }

    #[tracing::instrument(level = "trace")]
    fn expression(&mut self) {
        self.parse_precedence(Precedence::Assignment);
    }

    #[tracing::instrument(level = "trace")]
    fn expression_statement(&mut self) {
        self.expression();
        self.consume(TokenType::Semicolon, "expected ';' after expression");
        self.emit_operation(OpCode::Pop);
    }

    fn finish(mut self) -> Result<Rc<Function>, AggregateError> {
        if self.errors.is_empty() {
            self.emit_return();
            Ok(self.compiler.finish())
        } else {
            AggregateContext {
                inner: self.errors.into_boxed_slice(),
            }
            .fail()
        }
    }

    #[tracing::instrument(level = "trace")]
    fn for_statement(&mut self) {
        self.in_scope(|parser| {
            parser.consume(TokenType::LeftParen, "expected '(' after 'for'");
            if parser.r#match(TokenType::Semicolon) {
            } else if parser.r#match(TokenType::Var) {
                parser.var_declaration();
            } else {
                parser.expression_statement();
            }

            let mut loop_start = parser.current_chunk().current_offset();
            let exit_jump = if parser.r#match(TokenType::Semicolon) {
                None
            } else {
                parser.expression();
                parser.consume(TokenType::Semicolon, "expected ';' after loop condition");

                let exit_jump = parser.emit_jump(OpCode::JumpIfFalse);
                parser.emit_operation(OpCode::Pop);

                Some(exit_jump)
            };

            if !parser.r#match(TokenType::RightParen) {
                let body_jump = parser.emit_jump(OpCode::Jump);

                let increment_start = parser.current_chunk().current_offset();
                parser.expression();
                parser.emit_operation(OpCode::Pop);
                parser.consume(TokenType::RightParen, "expected ')' after for clauses");

                parser.emit_loop(loop_start);
                loop_start = increment_start;
                parser.patch_jump(body_jump);
            }

            parser.statement();

            parser.emit_loop(loop_start);

            if let Some(exit_jump) = exit_jump {
                parser.patch_jump(exit_jump);
                parser.emit_operation(OpCode::Pop);
            }
        });
    }

    #[tracing::instrument(level = "trace")]
    fn fun_declaration(&mut self) {
        let global = self.parse_variable("expected function name");
        self.compiler.mark_initialized();
        self.function(FunctionKind::Function);
        self.define_variable(global);
    }

    #[tracing::instrument(level = "trace")]
    fn function(&mut self, kind: FunctionKind) {
        let name = &self.previous().lexeme;
        let name = std::str::from_utf8(name).unwrap();
        let name = name.into();
        let enclosing = mem::replace(&mut self.compiler, Compiler::new(kind, name));
        self.compiler.begin_scope();

        self.consume(TokenType::LeftParen, "expected '(' after function name");
        if !self.check(TokenType::RightParen) {
            loop {
                self.compiler.function.arity += 1;
                if 255 < self.compiler.function.arity {
                    self.error_at_current("can't have more than 255 parameters");
                }

                let constant = self.parse_variable("expected parameter name");
                self.define_variable(constant);

                if !self.r#match(TokenType::Comma) {
                    break;
                }
            }
        }
        self.consume(TokenType::RightParen, "expected ')' after parameters");

        self.consume(TokenType::LeftBrace, "expected '{' before function body");
        self.block();

        self.emit_return();
        let compiler = mem::replace(&mut self.compiler, enclosing);
        let function = compiler.finish();
        self.emit_constant(function);
    }

    #[tracing::instrument(level = "trace")]
    fn grouping(&mut self, _: bool) {
        self.expression();
        self.consume(TokenType::RightParen, "expected ')' after expression");
    }

    fn identifier_constant(&mut self, name: &Bytes) -> usize {
        let name = std::str::from_utf8(name).unwrap();
        self.vm.insert_global(name.into())
    }

    #[tracing::instrument(level = "trace")]
    fn if_statement(&mut self) {
        self.consume(TokenType::LeftParen, "expected '(' after 'if'");
        self.expression();
        self.consume(TokenType::RightParen, "expect ')' after condition");

        let then_jump = self.emit_jump(OpCode::JumpIfFalse);
        self.emit_operation(OpCode::Pop);

        self.statement();

        let else_jump = self.emit_jump(OpCode::Jump);

        self.patch_jump(then_jump);
        self.emit_operation(OpCode::Pop);

        if self.r#match(TokenType::Else) {
            self.statement();
        }

        self.patch_jump(else_jump);
    }

    fn in_scope<F>(&mut self, block: F)
    where
        F: FnOnce(&mut Self),
    {
        self.compiler.begin_scope();
        block(self);
        let removed = self.compiler.end_scope();

        // remove local variables
        // TODO consider implementing PopN instruction
        for _ in 0..removed {
            self.emit_operation(OpCode::Pop);
        }
    }

    #[tracing::instrument(level = "trace")]
    fn literal(&mut self, _: bool) {
        match self.previous().r#type {
            TokenType::Nil => self.emit_operation(OpCode::Nil),
            TokenType::True => self.emit_operation(OpCode::True),
            TokenType::False => self.emit_operation(OpCode::False),

            _ => unreachable!(),
        }
    }

    fn r#match(&mut self, expected: TokenType) -> bool {
        if self.check(expected) {
            self.advance();
            true
        } else {
            false
        }
    }

    fn named_variable(&mut self, name: &Bytes, can_assign: bool) {
        let (index, getter, setter) = match self.compiler.resolve_local(name) {
            Err(message) => {
                self.error(message);
                return;
            }

            Ok(Some(index)) => (index, OpCode::GetLocal, OpCode::SetLocal),
            Ok(None) => (
                self.identifier_constant(name),
                OpCode::GetGlobal,
                OpCode::SetGlobal,
            ),
        };

        if can_assign && self.r#match(TokenType::Equal) {
            self.expression();
            self.emit_indexed(setter, index);
        } else {
            self.emit_indexed(getter, index);
        }
    }

    #[tracing::instrument(level = "trace")]
    fn number(&mut self, _: bool) {
        let value = &self.previous().lexeme;
        let value = std::str::from_utf8(value).unwrap();
        let value = value.parse::<f64>().unwrap();
        self.emit_constant(value);
    }

    #[tracing::instrument(level = "trace")]
    fn or(&mut self) {
        let else_jump = self.emit_jump(OpCode::JumpIfFalse);
        let end_jump = self.emit_jump(OpCode::Jump);

        self.patch_jump(else_jump);
        self.emit_operation(OpCode::Pop);

        self.parse_precedence(Precedence::Or);
        self.patch_jump(end_jump);
    }

    fn parse_precedence(&mut self, precedence: Precedence) {
        self.advance();
        let prefix_rule = if let Some(rule) = self.previous_rule().prefix {
            rule
        } else {
            self.error("expected expression");
            return;
        };

        let can_assign = precedence <= Precedence::Assignment;
        prefix_rule(self, can_assign);

        while precedence <= self.current_rule().precedence {
            self.advance();

            let infix_rule = self.previous_rule().infix;
            infix_rule.unwrap()(self);
        }

        if can_assign && self.r#match(TokenType::Equal) {
            self.error("invalid assignment target");
        }
    }

    fn parse_variable<S>(&mut self, error_message: &S) -> usize
    where
        S: ToString + ?Sized,
    {
        self.consume(TokenType::Identifier, error_message);

        self.declare_variable();
        if 0 < self.compiler.scope_depth {
            return 0;
        }

        self.identifier_constant(&self.previous().lexeme.clone())
    }

    fn patch_jump(&mut self, offset: usize) {
        if let Err(error) = self.current_chunk().patch_jump(offset) {
            self.error(error);
        }
    }

    fn previous(&self) -> &Token {
        self.previous.as_ref().unwrap()
    }

    fn previous_rule(&self) -> Rule<'vm> {
        Self::get_rule(self.previous().r#type)
    }

    #[tracing::instrument(level = "trace")]
    fn print_statement(&mut self) {
        self.expression();
        self.consume(TokenType::Semicolon, "expected ';' after value");
        self.emit_operation(OpCode::Print);
    }

    #[tracing::instrument(level = "trace")]
    fn return_statement(&mut self) {
        if let FunctionKind::Script = self.compiler.kind {
            self.error("can't return from top-level code");
        }

        if self.r#match(TokenType::Semicolon) {
            self.emit_return();
        } else {
            self.expression();
            self.consume(TokenType::Semicolon, "expected ';' after expression");
            self.emit_operation(OpCode::Return);
        }
    }

    #[tracing::instrument(level = "trace")]
    fn statement(&mut self) {
        if self.r#match(TokenType::Print) {
            self.print_statement();
        } else if self.r#match(TokenType::For) {
            self.for_statement();
        } else if self.r#match(TokenType::If) {
            self.if_statement();
        } else if self.r#match(TokenType::Return) {
            self.return_statement();
        } else if self.r#match(TokenType::While) {
            self.while_statement();
        } else if self.r#match(TokenType::LeftBrace) {
            self.in_scope(Self::block);
        } else {
            self.expression_statement();
        }
    }

    #[tracing::instrument(level = "trace")]
    fn string(&mut self, _: bool) {
        let value = &self.previous().lexeme;
        let value = &value[1..value.len() - 1];
        let value = std::str::from_utf8(value).unwrap();
        let value = Value::from(value);

        self.emit_constant(value);
    }

    fn synchronize(&mut self) {
        self.panic_mode = false;

        while self.current().r#type != TokenType::Eof {
            if self.previous().r#type == TokenType::Semicolon {
                return;
            }

            match self.current().r#type {
                TokenType::Class
                | TokenType::Fun
                | TokenType::Var
                | TokenType::For
                | TokenType::If
                | TokenType::While
                | TokenType::Print
                | TokenType::Return => {
                    return;
                }

                _ => {}
            }

            self.advance();
        }
    }

    #[tracing::instrument(level = "trace")]
    fn unary(&mut self, _: bool) {
        let operator = self.previous().r#type;

        self.parse_precedence(Precedence::Unary);

        match operator {
            TokenType::Bang => {
                self.emit_operation(OpCode::Not);
            }
            TokenType::Minus => {
                self.emit_operation(OpCode::Negate);
            }

            _ => unreachable!(),
        }
    }

    #[tracing::instrument(level = "trace")]
    fn var_declaration(&mut self) {
        let global = self.parse_variable("expected variable name");

        if self.r#match(TokenType::Equal) {
            self.expression();
        } else {
            self.emit_operation(OpCode::Nil);
        }
        self.consume(
            TokenType::Semicolon,
            "expected ';' after variable declaration",
        );

        self.define_variable(global);
    }

    #[tracing::instrument(level = "trace")]
    fn variable(&mut self, can_assign: bool) {
        self.named_variable(&self.previous().lexeme.clone(), can_assign);
    }

    #[tracing::instrument(level = "trace")]
    fn while_statement(&mut self) {
        let loop_start = self.current_chunk().current_offset();

        self.consume(TokenType::LeftParen, "expected '(' after 'while'");
        self.expression();
        self.consume(TokenType::RightParen, "expected ')' after condition");

        let exit_jump = self.emit_jump(OpCode::JumpIfFalse);
        self.emit_operation(OpCode::Pop);
        self.statement();

        self.emit_loop(loop_start);

        self.patch_jump(exit_jump);
        self.emit_operation(OpCode::Pop);
    }

    fn get_rule(r#type: TokenType) -> Rule<'vm> {
        match r#type {
            TokenType::LeftParen => Rule {
                prefix: Some(&Parser::grouping),
                infix: Some(&Parser::call),
                precedence: Precedence::Call,
            },

            TokenType::Minus => Rule {
                prefix: Some(&Parser::unary),
                infix: Some(&Parser::binary),
                precedence: Precedence::Term,
            },
            TokenType::Plus => Rule {
                prefix: None,
                infix: Some(&Parser::binary),
                precedence: Precedence::Term,
            },
            TokenType::Star | TokenType::Slash => Rule {
                prefix: None,
                infix: Some(&Parser::binary),
                precedence: Precedence::Factor,
            },

            TokenType::Bang => Rule {
                prefix: Some(&Parser::unary),
                infix: None,
                precedence: Precedence::None,
            },

            TokenType::BangEqual | TokenType::EqualEqual => Rule {
                prefix: None,
                infix: Some(&Parser::binary),
                precedence: Precedence::Equality,
            },

            TokenType::Greater
            | TokenType::GreaterEqual
            | TokenType::Less
            | TokenType::LessEqual => Rule {
                prefix: None,
                infix: Some(&Parser::binary),
                precedence: Precedence::Comparison,
            },

            TokenType::Identifier => Rule {
                prefix: Some(&Parser::variable),
                infix: None,
                precedence: Precedence::None,
            },
            TokenType::String => Rule {
                prefix: Some(&Parser::string),
                infix: None,
                precedence: Precedence::None,
            },
            TokenType::Number => Rule {
                prefix: Some(&Parser::number),
                infix: None,
                precedence: Precedence::None,
            },

            TokenType::And => Rule {
                prefix: None,
                infix: Some(&Parser::and),
                precedence: Precedence::And,
            },

            TokenType::Nil | TokenType::True | TokenType::False => Rule {
                prefix: Some(&Parser::literal),
                infix: None,
                precedence: Precedence::None,
            },

            TokenType::Or => Rule {
                prefix: None,
                infix: Some(&Parser::or),
                precedence: Precedence::Or,
            },

            _ => Rule {
                prefix: None,
                infix: None,
                precedence: Precedence::None,
            },
        }
    }
}

#[derive(Copy, Clone, Debug, PartialEq, PartialOrd)]
enum Precedence {
    None,
    Assignment,
    Or,
    And,
    Equality,
    Comparison,
    Term,
    Factor,
    Unary,
    Call,
    Primary,
}

impl Precedence {
    fn next(self) -> Self {
        match self {
            Self::None => Self::Assignment,
            Self::Assignment => Self::Or,
            Self::Or => Self::And,
            Self::And => Self::Equality,
            Self::Equality => Self::Comparison,
            Self::Comparison => Self::Term,
            Self::Term => Self::Factor,
            Self::Factor => Self::Unary,
            Self::Unary => Self::Call,
            Self::Call => Self::Primary,
            Self::Primary => unreachable!(),
        }
    }
}

#[derive(Copy, Clone)]
struct Rule<'vm> {
    prefix: Option<&'vm dyn Fn(&mut Parser<'vm>, bool)>,
    infix: Option<&'vm dyn Fn(&mut Parser<'vm>)>,

    precedence: Precedence,
}
