use {
    bytes::{Buf as _, Bytes},
    if_chain::if_chain,
};

#[derive(Clone, Debug)]
pub struct Scanner {
    start: Bytes,
    current: Bytes,

    line: usize,
}

impl Scanner {
    pub fn new(source: Bytes) -> Self {
        Self {
            start: source.clone(),
            current: source,

            line: 1,
        }
    }

    fn advance(&mut self) {
        if self.current.has_remaining() {
            self.current.advance(1);
        }
    }

    fn check_keyword(&self, start: usize, rest: &[u8], r#type: TokenType) -> TokenType {
        if_chain! {
            if self.lexeme_len() == start + rest.len();
            if &self.start[start..self.lexeme_len()] == rest;
            then {
                r#type
            }
            else {
                TokenType::Identifier
            }
        }
    }

    fn error_token<A: Into<Bytes>>(&mut self, message: A) -> Token {
        let lexeme = message.into();

        Token::new(TokenType::Error, lexeme, self.line)
    }

    fn identifier(&mut self) -> Token {
        while self.peek().is_ascii_alphanumeric() || self.peek() == b'_' {
            self.advance();
        }

        self.make_token(self.identifier_type())
    }

    fn identifier_type(&self) -> TokenType {
        match self.start[0] {
            b'a' => self.check_keyword(1, b"nd", TokenType::And),
            b'c' => self.check_keyword(1, b"lass", TokenType::Class),
            b'e' => self.check_keyword(1, b"lse", TokenType::Else),
            b'f' if 1 < self.lexeme_len() => match self.start[1] {
                b'a' => self.check_keyword(2, b"lse", TokenType::False),
                b'o' => self.check_keyword(2, b"r", TokenType::For),
                b'u' => self.check_keyword(2, b"n", TokenType::Fun),
                _ => TokenType::Identifier,
            },
            b'i' => self.check_keyword(1, b"f", TokenType::If),
            b'n' => self.check_keyword(1, b"il", TokenType::Nil),
            b'o' => self.check_keyword(1, b"r", TokenType::Or),
            b'p' => self.check_keyword(1, b"rint", TokenType::Print),
            b'r' => self.check_keyword(1, b"eturn", TokenType::Return),
            b't' if 1 < self.lexeme_len() => match self.start[1] {
                b'h' => self.check_keyword(2, b"is", TokenType::This),
                b'r' => self.check_keyword(2, b"ue", TokenType::True),
                _ => TokenType::Identifier,
            },
            b's' => self.check_keyword(1, b"uper", TokenType::Super),
            b'v' => self.check_keyword(1, b"ar", TokenType::Var),
            b'w' => self.check_keyword(1, b"hile", TokenType::While),

            _ => TokenType::Identifier,
        }
    }

    fn is_at_end(&self) -> bool {
        !self.current.has_remaining()
    }

    fn lexeme_len(&self) -> usize {
        self.start.len() - self.current.len()
    }

    fn make_token(&mut self, r#type: TokenType) -> Token {
        let lexeme = self.start.split_to(self.lexeme_len());

        Token::new(r#type, lexeme, self.line)
    }

    fn number(&mut self) -> Token {
        while self.peek().is_ascii_digit() {
            self.advance();
        }

        if self.peek() == b'.' && self.peek_next().is_ascii_digit() {
            self.advance();

            while self.peek().is_ascii_digit() {
                self.advance();
            }
        }

        self.make_token(TokenType::Number)
    }

    fn peek(&self) -> u8 {
        if self.is_at_end() {
            0
        } else {
            self.current[0]
        }
    }

    fn peek_next(&self) -> u8 {
        if self.current.len() < 2 {
            0
        } else {
            self.current[1]
        }
    }

    fn r#match(&mut self, expected: u8) -> bool {
        if self.is_at_end() {
            return false;
        }

        if self.peek() != expected {
            return false;
        }

        self.advance();
        true
    }

    fn skip_whitespace(&mut self) {
        loop {
            match self.peek() {
                b' ' | b'\t' | b'\r' => {
                    self.advance();
                }
                b'\n' => {
                    self.line += 1;
                    self.advance();
                }

                b'/' if self.peek_next() == b'/' => {
                    while self.peek() != b'\n' && !self.is_at_end() {
                        self.advance();
                    }
                }

                _ => {
                    return;
                }
            }
        }
    }

    fn string(&mut self) -> Token {
        while self.peek() != b'"' && !self.is_at_end() {
            if self.peek() == b'\n' {
                self.line += 1;
            }

            self.advance();
        }

        if self.is_at_end() {
            return self.error_token("unterminated string");
        }

        self.advance();
        self.make_token(TokenType::String)
    }
}

impl Iterator for Scanner {
    type Item = Token;

    fn next(&mut self) -> Option<Self::Item> {
        self.skip_whitespace();
        self.start = self.current.clone();
        if self.is_at_end() {
            return Some(self.make_token(TokenType::Eof));
        }

        let c = self.current.get_u8();
        match c {
            b'(' => Some(self.make_token(TokenType::LeftParen)),
            b')' => Some(self.make_token(TokenType::RightParen)),
            b'{' => Some(self.make_token(TokenType::LeftBrace)),
            b'}' => Some(self.make_token(TokenType::RightBrace)),
            b';' => Some(self.make_token(TokenType::Semicolon)),
            b',' => Some(self.make_token(TokenType::Comma)),
            b'.' => Some(self.make_token(TokenType::Dot)),
            b'-' => Some(self.make_token(TokenType::Minus)),
            b'+' => Some(self.make_token(TokenType::Plus)),
            b'/' => Some(self.make_token(TokenType::Slash)),
            b'*' => Some(self.make_token(TokenType::Star)),

            b'!' if self.r#match(b'=') => Some(self.make_token(TokenType::BangEqual)),
            b'!' => Some(self.make_token(TokenType::Bang)),
            b'=' if self.r#match(b'=') => Some(self.make_token(TokenType::EqualEqual)),
            b'=' => Some(self.make_token(TokenType::Equal)),
            b'<' if self.r#match(b'=') => Some(self.make_token(TokenType::LessEqual)),
            b'<' => Some(self.make_token(TokenType::Less)),
            b'>' if self.r#match(b'=') => Some(self.make_token(TokenType::GreaterEqual)),
            b'>' => Some(self.make_token(TokenType::Greater)),

            b'"' => Some(self.string()),

            b'0'..=b'9' => Some(self.number()),

            c if c.is_ascii_alphabetic() || c == b'_' => Some(self.identifier()),

            _ => Some(self.error_token("unexpected character")),
        }
    }
}

#[derive(Clone, Debug)]
pub struct Token {
    pub r#type: TokenType,
    pub lexeme: Bytes,

    pub line: usize,
}

impl Token {
    pub fn new(r#type: TokenType, lexeme: Bytes, line: usize) -> Self {
        Self {
            r#type,
            lexeme,

            line,
        }
    }
}

#[derive(Copy, Clone, Debug, PartialEq)]
#[repr(u8)]
pub enum TokenType {
    // Single-character tokens.
    LeftParen,
    RightParen,
    LeftBrace,
    RightBrace,
    Comma,
    Dot,
    Minus,
    Plus,
    Semicolon,
    Slash,
    Star,

    // One or two character tokens.
    Bang,
    BangEqual,
    Equal,
    EqualEqual,
    Greater,
    GreaterEqual,
    Less,
    LessEqual,

    // Literals.
    Identifier,
    String,
    Number,

    // Keywords.
    And,
    Class,
    Else,
    False,
    For,
    Fun,
    If,
    Nil,
    Or,
    Print,
    Return,
    Super,
    This,
    True,
    Var,
    While,

    Eof,
    Error,
}
