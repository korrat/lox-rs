use {
    lox_rs::chunk::{Chunk, OpCode},
    proptest::prelude::*,
};

prop_compose! {
    fn vec_and_index()(vec in prop::collection::vec(0..1000usize, 1..100))
                (index in (0..vec.len()), vec in Just(vec)) -> (Vec<usize>, usize) {
        (vec, index)
    }
}

proptest! {
    #[test]
    fn correct_offset((vec, offset) in vec_and_index()) {
        let mut builder = Chunk::builder();
        for &line in &vec {
            builder.write_operation(OpCode::Return, line);
        }
        let chunk = builder.finish();

        prop_assert_eq!(
            vec[offset],
            chunk.get_line(offset)
        );
    }
}
