use goldentests::{TestConfig, TestResult};

#[test]
pub fn assignment() -> TestResult<()> {
    let config = TestConfig::new("target/debug/lox-rs", "tests/ui/assignment/", "// ")?;

    config.run_tests()
}

#[test]
pub fn block() -> TestResult<()> {
    let config = TestConfig::new("target/debug/lox-rs", "tests/ui/block/", "// ")?;

    config.run_tests()
}

#[test]
pub fn bool() -> TestResult<()> {
    let config = TestConfig::new("target/debug/lox-rs", "tests/ui/bool/", "// ")?;

    config.run_tests()
}

#[test]
pub fn call() -> TestResult<()> {
    let config = TestConfig::new("target/debug/lox-rs", "tests/ui/call/", "// ")?;

    config.run_tests()
}

#[test]
pub fn comments() -> TestResult<()> {
    let config = TestConfig::new("target/debug/lox-rs", "tests/ui/comments/", "// ")?;

    config.run_tests()
}

#[test]
pub fn expressions() -> TestResult<()> {
    let config = TestConfig::new("target/debug/lox-rs", "tests/ui/expressions/", "// ")?;

    config.run_tests()
}

#[test]
pub fn r#for() -> TestResult<()> {
    let config = TestConfig::new("target/debug/lox-rs", "tests/ui/for/", "// ")?;

    config.run_tests()
}

#[test]
pub fn function() -> TestResult<()> {
    let config = TestConfig::new("target/debug/lox-rs", "tests/ui/function/", "// ")?;

    config.run_tests()
}

#[test]
pub fn r#if() -> TestResult<()> {
    let config = TestConfig::new("target/debug/lox-rs", "tests/ui/if/", "// ")?;

    config.run_tests()
}

#[test]
pub fn limit() -> TestResult<()> {
    let config = TestConfig::new("target/debug/lox-rs", "tests/ui/limit/", "// ")?;

    config.run_tests()
}

#[test]
pub fn logical_operator() -> TestResult<()> {
    let config = TestConfig::new("target/debug/lox-rs", "tests/ui/logical_operator/", "// ")?;

    config.run_tests()
}

#[test]
pub fn nil() -> TestResult<()> {
    let config = TestConfig::new("target/debug/lox-rs", "tests/ui/nil/", "// ")?;

    config.run_tests()
}

#[test]
pub fn number() -> TestResult<()> {
    let config = TestConfig::new("target/debug/lox-rs", "tests/ui/number/", "// ")?;

    config.run_tests()
}

#[test]
pub fn operator() -> TestResult<()> {
    let config = TestConfig::new("target/debug/lox-rs", "tests/ui/operator/", "// ")?;

    config.run_tests()
}

#[test]
pub fn other() -> TestResult<()> {
    let config = TestConfig::new("target/debug/lox-rs", "tests/ui/other/", "// ")?;

    config.run_tests()
}

#[test]
pub fn print() -> TestResult<()> {
    let config = TestConfig::new("target/debug/lox-rs", "tests/ui/print/", "// ")?;

    config.run_tests()
}

#[test]
pub fn r#return() -> TestResult<()> {
    let config = TestConfig::new("target/debug/lox-rs", "tests/ui/return/", "// ")?;

    config.run_tests()
}

#[test]
pub fn scanner() -> TestResult<()> {
    let config = TestConfig::new("target/debug/scanner", "tests/ui/scanning/", "// ")?;

    config.run_tests()
}

#[test]
pub fn string() -> TestResult<()> {
    let config = TestConfig::new("target/debug/lox-rs", "tests/ui/string/", "// ")?;

    config.run_tests()
}

#[test]
pub fn variable() -> TestResult<()> {
    let config = TestConfig::new("target/debug/lox-rs", "tests/ui/variable/", "// ")?;

    config.run_tests()
}

#[test]
pub fn r#while() -> TestResult<()> {
    let config = TestConfig::new("target/debug/lox-rs", "tests/ui/while/", "// ")?;

    config.run_tests()
}
