use bytes::{BufMut as _, BytesMut};
use lox_rs::{
    chunk::{Chunk, OpCode},
    debug,
};

#[test]
fn constant() {
    let chunk = Chunk::builder().write_constant(18.05, 1).finish();

    let mut out = BytesMut::new().writer();
    debug::disassemble_chunk(&mut out, &chunk, "test chunk");

    assert_eq!(
        out.into_inner(),
        "== test chunk ==\n0000    1 Constant            0 '18.05'\n"
    )
}

#[test]
fn constant_long() {
    let mut builder = Chunk::builder();
    for i in 0..256 {
        builder.write_constant(f64::from(i), 10);
    }
    builder.write_constant(23.3, 25);
    let chunk = builder.finish();

    let mut out = BytesMut::new().writer();
    debug::disassemble_chunk(&mut out, &chunk, "test chunk");

    assert!(out
        .into_inner()
        .ends_with(b"0512   25 ConstantLong      256 '23.3'\n"))
}

#[test]
fn r#return() {
    let chunk = Chunk::builder().write_operation(OpCode::Return, 1).finish();

    let mut out = BytesMut::new().writer();
    debug::disassemble_chunk(&mut out, &chunk, "test chunk");

    assert_eq!(out.into_inner(), "== test chunk ==\n0000    1 Return\n")
}

#[test]
fn chapter_14_final() {
    let chunk = Chunk::builder()
        .write_constant(1.2, 123)
        .write_operation(OpCode::Return, 123)
        .finish();

    let mut out = BytesMut::new().writer();
    debug::disassemble_chunk(&mut out, &chunk, "test chunk");

    assert_eq!(
        out.into_inner(),
        "== test chunk ==\n0000  123 Constant            0 '1.2'\n0002    | Return\n"
    );
}
